# Game The Jam 48

## Overview
"Game The Jam 48" is an experimental #meta game about participating in a game jam!

## Description
Keep alive the dev's spirit!

Your goal is to make a game for 48H GameJam by fully or partially completing 4 given tasks, while not allowing your character to give up. All tasks must be completed at least at 50% to win the game. If either the character's motivation or two other status parameters fall too low, the character will give up.


## Contact
For feedback, suggestions, or inquiries, please visit the [project page](https://tionard.itch.io/game-the-jam).
