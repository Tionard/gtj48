using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task : MonoBehaviour
{
    [Range (20,100)][SerializeField] protected float _goal = 100;
    [SerializeField] protected float _currentProgress = 0;
    [SerializeField] protected string _taskName;
    [SerializeField] protected float _progressRate;
    protected float _currentProgressRate = 0;
    protected bool _taskDone;

    protected void Start()
    {
        StartCoroutine(UpdateProgress());
    }

    public virtual void StartProgress()
    {
        _currentProgressRate = _progressRate;
    }

    public virtual void PauseProgress()
    {
        _currentProgressRate = 0;
    }

    public virtual void ReduceProgressByAmount(float amount)
    {
        _currentProgress = Mathf.Max(_currentProgress - amount, 0);
    }

    public virtual void IncreaseProgressByAmount(float amount)
    {
        _currentProgress = Mathf.Min(_currentProgress + amount, 100);
    }

    public virtual float GetCurrentProgressRate()
    {
        return _currentProgressRate;
    }

    public virtual float GetProgressRate()
    {
        return _progressRate;
    }


    public virtual float GetCurrentProgress()
    {
        return _currentProgress;
    }

    public virtual bool GetTaskDone()
    {
        return _taskDone;
    }

    protected virtual IEnumerator UpdateProgress()
    {
        while (!_taskDone)
        {
            if (!GameData._pause)
            {
                _currentProgress += _currentProgressRate;
                _currentProgress = Mathf.Min(_currentProgress, _goal);
                if(_currentProgress >= _goal)
                {
                    _taskDone = true;
                }
            }
            yield return new WaitForSeconds(GameData.GetGameStepSpeed());
        }
    }
}
