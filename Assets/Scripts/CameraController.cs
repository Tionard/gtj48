using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Camera cam;
    Vector3 _dragOrigin;

    [SerializeField] float _zoomStep, _minCamSize, _maxCamSize;
    [SerializeField] SpriteRenderer mapRenderer;

    float _mapMinX, _mapMaxX, _mapMinY, _mapMaxY;

    private void Awake()
    {
        //Set boundings for camera movement
        _mapMinX = mapRenderer.transform.position.x - mapRenderer.bounds.size.x;
        _mapMaxX = mapRenderer.transform.position.x + mapRenderer.bounds.size.x;
        _mapMinY = mapRenderer.transform.position.y - mapRenderer.bounds.size.y;
        _mapMaxY = mapRenderer.transform.position.y + mapRenderer.bounds.size.y;
    }

    void Update()
    {
        PanCamera();
        ChangeZoom();
        cam.transform.position = ClampCamera(cam.transform.position);
    }

    private void PanCamera()
    {
        //save position of moust in world space when drag starts
        if (Input.GetMouseButtonDown(2))
        {
            _dragOrigin = cam.ScreenToWorldPoint(Input.mousePosition);
        }

        //calculate distance between origin and current position
        if (Input.GetMouseButton(2))
        {
            Vector3 difference = _dragOrigin - cam.ScreenToWorldPoint(Input.mousePosition);

            //move cam by that distance
            cam.transform.position += difference;
        }
    }

    private void ChangeZoom()
    {
        if(Input.mouseScrollDelta.y > 0)
        {
            ZoomIn();
        }
        else if(Input.mouseScrollDelta.y < 0)
        {
            ZoomOut();
        }
    }

    private void ZoomIn()
    {
        float newSize = cam.orthographicSize - _zoomStep;

        cam.orthographicSize = Mathf.Clamp(newSize, _minCamSize, _maxCamSize);
    }

    private void ZoomOut()
    {
        float newSize = cam.orthographicSize + _zoomStep;

        cam.orthographicSize = Mathf.Clamp(newSize, _minCamSize, _maxCamSize);

        
    }

    private Vector3 ClampCamera(Vector3 targetPosition)
    {
        float camHeight = cam.orthographicSize;
        float camWidth = camHeight * cam.aspect;

        float minCamX = _mapMinX + camWidth;
        float maxCamX = _mapMaxX - camWidth;
        float minCamY = _mapMinX + camHeight;
        float maxCamY = _mapMaxX - camHeight;

        float newCamX = Mathf.Clamp(targetPosition.x, minCamX, maxCamX);
        float newCamY = Mathf.Clamp(targetPosition.y, minCamY, maxCamY);

        return new Vector3(newCamX, newCamY, targetPosition.z);
    }
}
