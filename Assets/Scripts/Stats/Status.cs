using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Status : MonoBehaviour
{
    [Range(0,100)][SerializeField] protected float _value;
    [Range(0, 1)][SerializeField] protected float _reductionSpeed;
    protected float _speedModifier = 0;

    public Status() {}

    public Status(float value, float reductionSpeed)
    {
        _value = value;
        _reductionSpeed = reductionSpeed;
    }

    private void Start()
    {
        StartCoroutine(UpdateStatus());
    }

    public virtual float GetValue()
    {
        return _value;
    }

    public virtual float GetReductionSpeed()
    {
        return _reductionSpeed;
    }

    public virtual float GetTotalSpeed()
    {
        return (_reductionSpeed + _speedModifier);
    }

    public virtual void SetSpeedModifier(float value)
    {
        _speedModifier = value;
    }

    public virtual void ResetSpeedModifier()
    {
        _speedModifier = 0;
    }

    protected virtual IEnumerator UpdateStatus()
    {
        while (true)
        {       
            if (!GameData._pause)
            {
                _value -= (_reductionSpeed + _speedModifier);
                _value = Mathf.Clamp(_value, 0, 100);
            }
            yield return new WaitForSeconds(GameData.GetGameStepSpeed());
        }
    }
}
