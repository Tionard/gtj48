using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusManager : MonoBehaviour
{
    [SerializeField] Status motivationStatus;
    [SerializeField] Status energyStatus;
    [SerializeField] Status moodStatus;
    [SerializeField] Status hungerStatus;
    [SerializeField] Status bladderStatus;

    private void Awake()
    {
        GameData._statusManager = this;
    }

    public Status GetMotivationStatus()
    {
        return motivationStatus;
    }

    public Status GetEnergyStatus()
    {
        return energyStatus;
    }

    public Status GetMoodStatus()
    {
        return moodStatus;
    }
    public Status GetHungerStatus()
    {
        return hungerStatus;
    }

    public Status GetBladderStatus()
    {
        return bladderStatus;
    }

}
