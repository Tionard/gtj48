using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    [SerializeField] Animator _bgAnimator;
    void Awake()
    {
        GameData._animationManager = this;
    }

    public Animator GetAnimator()
    {
        return _bgAnimator;
    }

    private void Update()
    {
        //test area
        if (Input.GetKeyDown(KeyCode.Alpha1)) { _bgAnimator.SetTrigger("Work"); }
        if (Input.GetKeyDown(KeyCode.Alpha2)) { _bgAnimator.SetTrigger("Eat"); }
        if (Input.GetKeyDown(KeyCode.Alpha3)) { _bgAnimator.SetTrigger("Sleep"); }
        if (Input.GetKeyDown(KeyCode.Alpha4)) { _bgAnimator.SetTrigger("Play"); }
        if (Input.GetKeyDown(KeyCode.Alpha5)) { _bgAnimator.SetTrigger("WC"); }
    }

}
