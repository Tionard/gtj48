using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    [SerializeField] GameObject taskSelectorPopup;
    [SerializeField] GameObject foodSelectorPopup;
    [SerializeField] GameObject confirmationPopup;
    [SerializeField] GameObject welcomePopup;
    [SerializeField] HighlightActiveIcon highlightSpeedState;


    private void ChangeToOpenPopupState()
    {
        GameData.PauseTime();
        highlightSpeedState.HighlightOnlyOneElement(0);
        GameData._activePopup = true;
    }

    private void ChangeToClosedPopupState()
    {
        GameData.UnpauseTime();        
        highlightSpeedState.HighlightOnlyOneElement((int)GameData._gameSpeed/GameData._defaultGameSpeed);
        GameData._activePopup = false;
    }

    public void OpenTaskSelector()
    {
        ChangeToOpenPopupState();
        taskSelectorPopup.SetActive(true);
    }

    public void CloseTaskSelector()
    {
        ChangeToClosedPopupState();
        taskSelectorPopup.SetActive(false);
    }

    public void OpenWelcomePopup()
    {
        ChangeToOpenPopupState();
        welcomePopup.SetActive(true);
    }

    public void CloseWelcomePopup()
    {
        ChangeToClosedPopupState();
        welcomePopup.SetActive(false);
    }

    public void OpenFoodSelector()
    {
        ChangeToOpenPopupState();
        foodSelectorPopup.SetActive(true);
    }

    public void CloseFoodSelector()
    {
        ChangeToClosedPopupState();
        foodSelectorPopup.SetActive(false);
    }

    public void OpenConfirmationPopup()
    {
        ChangeToOpenPopupState();
        confirmationPopup.SetActive(true);
    }

    public void CloseConfirmationPopup()
    {
        ChangeToClosedPopupState();
        confirmationPopup.SetActive(false);
    }
}
