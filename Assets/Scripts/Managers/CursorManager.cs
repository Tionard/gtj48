using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    [SerializeField] Texture2D cursorArrow;
    [SerializeField] Texture2D cursorHold;
    [SerializeField] Texture2D cursorHover;
    [SerializeField] AudioSource audio;

    private void Start()
    {
        Cursor.SetCursor(cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
    }

    private void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            audio.Play();
        }

        if (Input.GetMouseButtonDown(2))
        {
            Cursor.SetCursor(cursorHold, Vector2.zero, CursorMode.ForceSoftware);
        }

        if (Input.GetMouseButtonUp(2))
        {
            Cursor.SetCursor(cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
        }
    }
}
