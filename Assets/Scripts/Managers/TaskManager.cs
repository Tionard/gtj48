using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager : MonoBehaviour
{
    [SerializeField] Task gameDesignTask;
    [SerializeField] Task gameplayTask;
    [SerializeField] Task uITask;
    [SerializeField] Task soundTask;

    private void Awake()
    {
        GameData._taskManager = this;
    }

    public void StartGameDesignTask()
    {
        PrepareForNewTask();
        gameDesignTask.StartProgress();
    }

    public void StartGameplayTask()
    {
        PrepareForNewTask();
        gameplayTask.StartProgress();
    }

    public void StartUITask()
    {
        PrepareForNewTask();
        uITask.StartProgress();
    }

    public void StartSoundTask()
    {
        PrepareForNewTask();

        soundTask.StartProgress();
    }

    private void PrepareForNewTask()
    {
        GameData._animationManager.GetAnimator().SetTrigger("Work");
        PauseAllTasks();
    }

    public void PauseAllTasks()
    {
        gameDesignTask.PauseProgress();
        gameplayTask.PauseProgress();
        uITask.PauseProgress();
        soundTask.PauseProgress();
    }

    public Task GetGameDesignTask()
    {
        return gameDesignTask;
    }
    public Task GetGameplayTask()
    {
        return gameplayTask;
    }
    public Task GetUITask()
    {
        return uITask;
    }
    public Task GetSoundTask()
    {
        return soundTask;
    }
}
