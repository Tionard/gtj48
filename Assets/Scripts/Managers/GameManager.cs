using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Final Score")]
    [SerializeField] GameObject _finalPopup;
    [SerializeField] GameObject _winningLabel;
    [SerializeField] GameObject _loosingLabel;
    [SerializeField] TMP_Text _gameDesignGrade;
    [SerializeField] TMP_Text _gameplayGrade;
    [SerializeField] TMP_Text _uiGrade;
    [SerializeField] TMP_Text _soundGrade;

    [Header("Losing Settings")]
    [SerializeField] GameObject _room;
    [SerializeField] GameObject _heroDied;
    [SerializeField] GameObject _loosingPopup;

    [Header("UI")]
    [SerializeField] GameObject _infoUI;

    [Header("Managers")]
    [SerializeField] TaskManager _taskManager;
    [SerializeField] StatusManager _statusManager;

    private bool _gameEnded;

    float _motivation, _energie, _mood, _hunger, _bladder;


    private void Update()
    {
        if (_gameEnded) return;

        if (GameData._timeUntilDeadline <= 0)
        {
            EvaluateGameJamRun();
        }
        else
        {
            CheckForFails();
        }
    }

    public void RestartScene()
    {
        _room.SetActive(true);
        _loosingPopup.SetActive(false);
        _finalPopup.SetActive(false);
        GameData._timeUntilDeadline = 2880;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void CheckForFails()
    {
        float failedStats = 0;

        _motivation = _statusManager.GetMotivationStatus().GetValue();
        _energie = _statusManager.GetEnergyStatus().GetValue();
        _mood = _statusManager.GetMoodStatus().GetValue();
        _hunger = _statusManager.GetHungerStatus().GetValue();
        _bladder = _statusManager.GetBladderStatus().GetValue();

        if (_motivation < 10)
        {
            failedStats += 2;
        }
        if (_energie < 10)
        {
            failedStats++;
        }
        if (_mood < 10)
        {
            failedStats++;
        }
        if (_hunger < 10)
        {
            failedStats++;
        }
        if (_bladder < 10)
        {
            failedStats++;
        }

        if (failedStats >= 2)
        {
            GameData.PauseTime();
            GameData._activePopup = true;

            _room.SetActive(false);
            _heroDied.SetActive(true);
            _loosingPopup.SetActive(true);
            _infoUI.SetActive(false);
            _gameEnded = true;
        }
    }

    private void EvaluateGameJamRun()
    {
        GameData.PauseTime();
        GameData._activePopup = true;

        var gameDesignProgress = _taskManager.GetGameDesignTask().GetCurrentProgress();
        var gameplayProgress = _taskManager.GetGameplayTask().GetCurrentProgress();
        var iuProgress = _taskManager.GetUITask().GetCurrentProgress();
        var soundProgress = _taskManager.GetSoundTask().GetCurrentProgress();

        string gameDesignGrade = EvaluateProgress(gameDesignProgress);
        string gameplayGrade = EvaluateProgress(gameplayProgress);
        string iuGrade = EvaluateProgress(iuProgress);
        string soundGrade = EvaluateProgress(soundProgress);

        _gameDesignGrade.text = "Game Design: " + gameDesignGrade;
        _gameplayGrade.text = "Gameplay: " + gameplayGrade;
        _uiGrade.text = "UI Design: " + iuGrade;
        _soundGrade.text = "Sound: " + soundGrade;

        if(gameDesignProgress < 50 || gameplayProgress < 50|| iuProgress < 50 || soundProgress < 50)
        {
            _loosingLabel.SetActive(true);
        }
        else
        {
            _winningLabel.SetActive(true);
        }

        _finalPopup.SetActive(true);
        _infoUI.SetActive(false);
        _room.SetActive(false);
        _gameEnded = true;
    }

    private string EvaluateProgress(float progress)
    {

        if (progress < 49.5f)
            return "F";
        else if (progress < 59.5f)
            return "E";
        else if (progress < 69.5f)
            return "D";
        else if (progress < 79.5f)
            return "C";
        else if (progress < 89.5f)
            return "B";
        else if (progress < 96.5f)
            return "A";
        else if (progress < 101)
            return "S";

        return "F";
    }
}
