using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeManager : MonoBehaviour
{

    [SerializeField] Clock _clock;
    [SerializeField] TMP_Text clockDisplay;
    [Range(1,8)][SerializeField] float _gameSpeed = 1;
    [SerializeField] bool _pause = true;

    // Start is called before the first frame update
    void Awake()
    {
        GameData._timeManager = this;
        _clock = new Clock(1, 17, 0);
    }


    void Start()
    {
        StartCoroutine(UpdateTime());
        GameData._pause = _pause;
        GameData._gameSpeed = _gameSpeed;
    }

    IEnumerator UpdateTime()
    {
        while (true)
        {
            if (!GameData._pause)
            {
                _clock.AddMinute();
                GameData._timeUntilDeadline--;
                clockDisplay.text = _clock.GetTimeAsText();
                int currentHour = _clock.GetHour();
                if (currentHour >= 6 && currentHour < 20)
                    GameData._night = false;
                else
                    GameData._night = true;
                
                GameData._animationManager.GetAnimator().SetBool("Night", GameData._night);

            }
            yield return new WaitForSeconds(GameData.GetGameStepSpeed());
        }
    }

    public void SetStandartSpeed()
    {
        GameData._pause = false;
        GameData._gameSpeed = GameData._defaultGameSpeed;
    }

    public void SetFastSpeed()
    {
        GameData._pause = false;
        GameData._gameSpeed = GameData._fastGameSpeed;
    }

    public void PauseTime()
    {
        GameData._pause = true;
    }
}

internal class Clock
{
    [SerializeField] int _day;
    int _hour;
    int _minute;
    const int _minutesInHour = 60;
    const int _hoursInDay = 24;
    Dictionary<string,int> _timers = new Dictionary<string,int>();

    public Clock(int day, int hour, int minute)
    {
        _day = day;
        _hour = hour;
        _minute = minute;
    }

    public int getDay()
    {
        return _day;
    }

    public int GetHour() 
    {
        return _hour;
    }

    public void SetTimer(string name, int minutes)
    {
        if (_timers.ContainsKey(name))
        {
            _timers[name] = minutes;
        }
        else
        {
            _timers.Add(name, minutes);
        }
    }
    public bool TimerExists(string name)
    {
        if (!_timers.ContainsKey(name)) return false;
        else return true;
    }

    public bool TimerActive(string name)
    {
        if (!TimerExists(name)) return false;
        else return (_timers[name] > 0);
    }

    public int GetMinute()
    {
        return _minute;
    }

    private void AddMinutes(int amount)
    {
        _minute += amount;
        StructureTime();
        foreach(string timerName in _timers.Keys)
        {
            if(_timers[timerName] > 0)
            {
                _timers[timerName] -= amount;
            }
        }
    }

    private void AddHour()
    {
        AddMinutes(_minutesInHour);
    }

    private void AddDay()
    {
        AddMinutes(_minutesInHour * _hoursInDay);
    }

    public void AddMinute()
    {
        AddMinutes(1);
    }

    public string GetTimeAsText()
    {
        var currentTime = "DAY " + _day + "\n";

        if(_hour < 10)
        {
            currentTime += "0";
        }
        currentTime += _hour +":";

        if (_minute < 10)
        {
            currentTime += "0";
        }
        currentTime += _minute;

        return currentTime;
    }
    
    private void StructureTime()
    {
        int hoursSkipped = 0;
        while (_minute > _minutesInHour - 1)
        {
            _minute -= _minutesInHour;
            hoursSkipped++;
        }

        _hour += hoursSkipped;

        int daysSkipped = 0;
        if (_hour > _hoursInDay - 1)
        {
            _hour -= _hoursInDay;
            daysSkipped++;
        }
        _day += daysSkipped;
    }

}
