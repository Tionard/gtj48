using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData
{
    public static float _gameSpeed = 4f;
    public static bool _pause = true;
    public static bool _night;
    public static bool _activePopup;
    public static Activator _lastActivator;
    public static int _timeUntilDeadline = 2880;

    public const int _defaultGameSpeed = 4;
    public const int _fastGameSpeed = 8;

    public static TaskManager _taskManager;
    public static StatusManager _statusManager;
    public static TimeManager _timeManager;
    public static AnimationManager _animationManager;

    public static float GetGameStepSpeed()
    {
        return 1f / _gameSpeed;
    }

    public static void PauseTime()
    {
        _pause = true;
    }

    public static void UnpauseTime()
    {
        _pause = false;
    }

}
