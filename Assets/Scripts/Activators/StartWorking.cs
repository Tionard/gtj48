using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartWorking : Activator
{
    [SerializeField] float energyDebuf;
    [SerializeField] float moodDebuf;
    [SerializeField] float motivationDebuf;
    [SerializeField] float hungerDebuf;
    [SerializeField] float bladderDebuf;
    [SerializeField] PopupManager popupManager;

    public override void StartCustomAction()
    {
        if (GameData._lastActivator != null) GameData._lastActivator.PauseCustomAction();

        base.StartCustomAction();
        GameData._taskManager.PauseAllTasks();
        GameData._animationManager.GetAnimator().SetTrigger("Work");

        popupManager.OpenTaskSelector();

        GameData._statusManager.GetEnergyStatus().SetSpeedModifier(energyDebuf);
        GameData._statusManager.GetMoodStatus().SetSpeedModifier(moodDebuf);
        GameData._statusManager.GetHungerStatus().SetSpeedModifier(hungerDebuf);
        GameData._statusManager.GetBladderStatus().SetSpeedModifier(bladderDebuf);
        GameData._statusManager.GetMotivationStatus().SetSpeedModifier(motivationDebuf);

        GameData._lastActivator = this;
    }

    public override void PauseCustomAction()
    {
        base.PauseCustomAction();
        GameData._statusManager.GetEnergyStatus().ResetSpeedModifier();
        GameData._statusManager.GetMotivationStatus().ResetSpeedModifier();
        GameData._statusManager.GetMoodStatus().ResetSpeedModifier();
        GameData._statusManager.GetHungerStatus().ResetSpeedModifier();
        GameData._statusManager.GetBladderStatus().ResetSpeedModifier();
    }
}
