using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartEating : Activator
{
    [SerializeField] float energyBoostPizza;
    [SerializeField] float moodBoostPizza;
    [SerializeField] float motivationDebuffPizza;
    [SerializeField] float hungerBoostPizza;
    [SerializeField] float bladderDebufPizza;
    [SerializeField] PopupManager popupManager;
    [SerializeField] float energyBoostCoffee;
    [SerializeField] float moodDebuffCoffee;
    [SerializeField] float motivatonBoost;
    [SerializeField] float hungerDebuffCoffee;
    [SerializeField] float bladderDebuffCoffe;

    public override void StartCustomAction()
    { 
        if (GameData._lastActivator != null) GameData._lastActivator.PauseCustomAction();

        base.StartCustomAction();
        GameData._taskManager.PauseAllTasks();
        GameData._animationManager.GetAnimator().SetTrigger("Eat");

        popupManager.OpenFoodSelector();

        GameData._lastActivator = this;
    }

    public void EatPizza()
    {
        GameData._statusManager.GetEnergyStatus().SetSpeedModifier(-energyBoostPizza);
        GameData._statusManager.GetMoodStatus().SetSpeedModifier(-moodBoostPizza);
        GameData._statusManager.GetHungerStatus().SetSpeedModifier(-hungerBoostPizza);
        GameData._statusManager.GetBladderStatus().SetSpeedModifier( bladderDebufPizza);
        GameData._statusManager.GetMotivationStatus().SetSpeedModifier(motivationDebuffPizza);
    }

    public void DrinkCoffee()
    {
        GameData._statusManager.GetEnergyStatus().SetSpeedModifier(-energyBoostCoffee);
        GameData._statusManager.GetMoodStatus().SetSpeedModifier( moodDebuffCoffee);
        GameData._statusManager.GetHungerStatus().SetSpeedModifier(hungerDebuffCoffee);
        GameData._statusManager.GetBladderStatus().SetSpeedModifier(bladderDebuffCoffe);
        GameData._statusManager.GetMotivationStatus().SetSpeedModifier(-motivatonBoost);
    }



    public override void PauseCustomAction()
    {
        base.PauseCustomAction();
        GameData._statusManager.GetEnergyStatus().ResetSpeedModifier();
        GameData._statusManager.GetMotivationStatus().ResetSpeedModifier();
        GameData._statusManager.GetMoodStatus().ResetSpeedModifier();
        GameData._statusManager.GetHungerStatus().ResetSpeedModifier();
        GameData._statusManager.GetBladderStatus().ResetSpeedModifier();
    }
}
