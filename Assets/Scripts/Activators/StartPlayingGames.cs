using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPlayingGames : Activator
{
    [SerializeField] float energyDebuf;
    [SerializeField] float moodBoost;
    [SerializeField] float motivationBoost;
    [SerializeField] float hungerDebuf;
    [SerializeField] float bladderDebuf;

    public override void StartCustomAction()
    {
        if (GameData._lastActivator != null) GameData._lastActivator.PauseCustomAction();

        base.StartCustomAction();
        GameData._taskManager.PauseAllTasks();
        GameData._animationManager.GetAnimator().SetTrigger("Play");

        GameData._statusManager.GetEnergyStatus().SetSpeedModifier(energyDebuf);
        GameData._statusManager.GetMoodStatus().SetSpeedModifier(- moodBoost);
        GameData._statusManager.GetMotivationStatus().SetSpeedModifier(-motivationBoost);
        GameData._statusManager.GetHungerStatus().SetSpeedModifier(hungerDebuf);
        GameData._statusManager.GetBladderStatus().SetSpeedModifier(bladderDebuf);

        GameData._lastActivator = this;
    }

    public override void PauseCustomAction()
    {
        base.PauseCustomAction();
        GameData._statusManager.GetEnergyStatus().ResetSpeedModifier();
        GameData._statusManager.GetMoodStatus().ResetSpeedModifier();
        GameData._statusManager.GetMotivationStatus().ResetSpeedModifier();
        GameData._statusManager.GetHungerStatus().ResetSpeedModifier();
        GameData._statusManager.GetBladderStatus().ResetSpeedModifier();
    }
}
