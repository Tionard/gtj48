using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSleeping : Activator
{
    [SerializeField] float energyBoost;
    [SerializeField] float motivationBoost;
    [SerializeField] float moodBoost;
    [SerializeField] float hungerBoost;

    public override void StartCustomAction()
    {
        if(GameData._lastActivator != null) GameData._lastActivator.PauseCustomAction();

        base.StartCustomAction();
        GameData._taskManager.PauseAllTasks();
        GameData._animationManager.GetAnimator().SetTrigger("Sleep");
        GameData._statusManager.GetEnergyStatus().SetSpeedModifier(- energyBoost);
        GameData._statusManager.GetMotivationStatus().SetSpeedModifier(-motivationBoost);
        GameData._statusManager.GetMoodStatus().SetSpeedModifier(- moodBoost);
        GameData._statusManager.GetHungerStatus().SetSpeedModifier(- hungerBoost);

        GameData._lastActivator = this;
    }

    public override void PauseCustomAction()
    {
        base.PauseCustomAction();
        GameData._statusManager.GetEnergyStatus().ResetSpeedModifier();
        GameData._statusManager.GetMotivationStatus().ResetSpeedModifier();
        GameData._statusManager.GetMoodStatus().ResetSpeedModifier();
        GameData._statusManager.GetHungerStatus().ResetSpeedModifier();
    }
}
