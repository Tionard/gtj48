using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Activator : MonoBehaviour
{
    public virtual void StartCustomAction()
    {

    }

    public virtual void PauseCustomAction()
    {

    }
}
