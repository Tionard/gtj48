using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTaskSelector : Activator
{
    [SerializeField] PopupManager popupManager;
    public override void StartCustomAction()
    {
        if (GameData._lastActivator != null) GameData._lastActivator.PauseCustomAction();

        base.StartCustomAction();
        popupManager.OpenTaskSelector();

        GameData._lastActivator = this;
    }
}
