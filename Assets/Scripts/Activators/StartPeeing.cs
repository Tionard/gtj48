using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPeeing : Activator
{
    [SerializeField] float motivationBoost;
    [SerializeField] float bladderBoost;

    public override void StartCustomAction()
    {
        if (GameData._lastActivator != null) GameData._lastActivator.PauseCustomAction();

        base.StartCustomAction();
        GameData._taskManager.PauseAllTasks();
        GameData._animationManager.GetAnimator().SetTrigger("WC");

        GameData._statusManager.GetMotivationStatus().SetSpeedModifier(-motivationBoost);
        GameData._statusManager.GetBladderStatus().SetSpeedModifier(-bladderBoost);

        GameData._lastActivator = this;
    }

    public override void PauseCustomAction()
    {
        base.PauseCustomAction();

        GameData._statusManager.GetMotivationStatus().ResetSpeedModifier();
        GameData._statusManager.GetBladderStatus().ResetSpeedModifier();
    }
}
