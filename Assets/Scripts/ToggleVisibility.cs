using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ToggleVisibility : MonoBehaviour
{
    [SerializeField] SpriteRenderer sprite;
    [SerializeField] Camera cam;
    [SerializeField] Activator activator;

    private void Update()
    {
        ActivateOnHover();
    }

    private void ActivateOnHover()
    {
        RaycastHit2D hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        if (!EventSystem.current.IsPointerOverGameObject() && hit != null && hit.transform == this.transform)
        {
            sprite.enabled = true;
            if (activator != null && Input.GetMouseButtonDown(0) && !GameData._activePopup)
            {
                activator.StartCustomAction();
            }
        }
        else
        {
            sprite.enabled = false;
        }
    }

}
