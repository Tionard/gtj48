using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ActiveTaskGrowthIndicator : MonoBehaviour
{
    [SerializeField] TaskDisplay task;
    [SerializeField] TMP_Text indicator;

    private void Update()
    {
        if(task.GetTask().GetCurrentProgressRate() > 0)
        {
            indicator.text = ">>>";
        }
        else
        {
            indicator.text = "";
        }
    }
}
