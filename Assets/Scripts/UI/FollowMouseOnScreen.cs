using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FollowMouseOnScreen : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] GameObject tooltip;
    [SerializeField] TMP_Text texttip;
    string _objName;
    // Update is called once per frame
    void Update()
    {
        Vector3 mousPos = Input.mousePosition;
        transform.position = new Vector3(mousPos.x, mousPos.y + (Screen.height/38.4f), mousPos.z);
        ActivateOnHover();
    }


    private void ActivateOnHover()
    {
        RaycastHit2D hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        if (hit != null && hit.transform?.gameObject?.layer == 6)
        {
            if (tooltip.activeSelf) return;
            tooltip.SetActive(true);
            texttip.text = hit.transform.name;
        }
        else
        {
            if (!tooltip.activeSelf) return;
            tooltip.SetActive(false);
        }
    }
}
