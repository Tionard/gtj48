using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighlightActiveIcon : MonoBehaviour
{
    [SerializeField] Color standartColor;
    [SerializeField] Color highlightColor;
    [SerializeField] GameObject[] elements;

    public void HighlightOnlyOneElement(int id)
    {
        foreach(GameObject element in elements)
        {
            element.GetComponent<Image>().color = standartColor;
        }
        elements[id].GetComponent<Image>().color = highlightColor;
    }
}
