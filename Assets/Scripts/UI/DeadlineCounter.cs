using System.Collections;
using TMPro;
using UnityEngine;

public class DeadlineCounter : MonoBehaviour
{
    [SerializeField] TMP_Text deadlineTimerText;

    private void Start()
    {
        StartCoroutine(UpdateDeadlineTimer());
    }

    IEnumerator UpdateDeadlineTimer()
    {
        while (true)
        {
            if (!GameData._pause)
            {
                int time = Mathf.RoundToInt(GameData._timeUntilDeadline / 60f);
                if(time > 1)
                {
                    deadlineTimerText.text = "~" + time + " Hours";
                }
                else
                {
                    deadlineTimerText.text = GameData._timeUntilDeadline + " Minutes";
                }

            }
            yield return new WaitForSeconds(GameData.GetGameStepSpeed());
        }
    }

}
