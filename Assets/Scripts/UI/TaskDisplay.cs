using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TaskDisplay : MonoBehaviour
{
    [SerializeField] Task task;
    [SerializeField] TMP_Text precentage;
    [SerializeField] Gradient progressGradient;

    private void Update()
    {
        precentage.text = Mathf.Round(task.GetCurrentProgress()) + "%";
        precentage.color = progressGradient.Evaluate(task.GetCurrentProgress() / 100f);
    }

    public Task GetTask()
    {
        return task;
    }
}
