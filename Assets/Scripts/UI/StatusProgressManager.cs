using UnityEngine;
using UnityEngine.UI;

public class StatusProgressManager : MonoBehaviour
{

    [SerializeField] Gradient _statusColor;
    [SerializeField] bool _invertGradient;
    [SerializeField] Image _statusFill;
    [SerializeField] Slider _statusProgress;
    [SerializeField] Status _status;

    private void Update()
    {
        UpdateValue();
        UpdateColor();
    }
    
    private void UpdateValue()
    {
        if (_status == null) return;
        _statusProgress.value = _status.GetValue();
    }

    private void UpdateColor()
    {
        float keyValue = _statusProgress.value/100;

        if (_invertGradient)
            keyValue = 1 - keyValue;

        _statusFill.color = _statusColor.Evaluate(keyValue);
    }

    public Status GetStatus()
    {
        return _status;
    }

}
