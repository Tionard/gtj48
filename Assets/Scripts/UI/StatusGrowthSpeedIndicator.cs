using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StatusGrowthSpeedIndicator : MonoBehaviour
{
    Color _colorPositive = Color.green;
    Color _colorNegative = Color.red;
    [SerializeField] float _speed = 0;
    [SerializeField] bool _direction = true;
    [SerializeField] TMP_Text _indicator;
    [SerializeField] StatusProgressManager statusManager;

    private void Update()
    {
        ChangeStatus();
    }

    public void ChangeStatus()
    {
        _speed = statusManager.GetStatus().GetTotalSpeed();

        if (_speed < 0)
        {
            _indicator.text = ">";
            if (_speed <= 0.2)
                _indicator.text = ">>";
            if (_speed <= 0.4)
                _indicator.text = ">>>";
            if (_speed <= 0.6)
                _indicator.text = ">>>>";
            _indicator.color = _colorPositive;
        }
        else if (_speed > 0)
        {
            _indicator.text = "<";

            if (_speed >= 0.2)
                _indicator.text = "<<";
            if (_speed >= 0.4)
                _indicator.text = "<<<";
            if (_speed >= 0.6)
                _indicator.text = "<<<<";
            _indicator.color = _colorNegative;
        }


    }
}
