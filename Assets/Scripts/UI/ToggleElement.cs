using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleElement : MonoBehaviour
{
    [SerializeField] GameObject state1;
    [SerializeField] GameObject state2;

    public void ToggleState()
    {
        state1.SetActive(state2.activeSelf);
        state2.SetActive(!state1.activeSelf);
    }
}
